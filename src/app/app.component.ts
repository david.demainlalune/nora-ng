import { Component } from '@angular/core';

import { NoraService } from '../../module/nora.service'
import { I_AssetSequencer } from '../../module/model/i_AssetSequencer'
import { PlaylistInfo } from '../../module/model/playlistInfo'
import { AudioAttachment } from '../../module/model/audioAttachment';

import { NarrativeArcSequence } from '../../module/model/narrativeArcSequence'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [NoraService]
})
export class AppComponent {
  title = 'app works!';
  constructor(private nora_service:NoraService){

  this.nora_service.init()
    .then(() => {

      // this.test_static_playlist();
      this.test_dynamic_playlist();

    });
  }

  test_dynamic_playlist():void{
      let points = [
          [
            6.134469509124755,
            46.199713216791835
          ],
          [
            6.134469509124755,
            46.19959254530652
          ],
          [
            6.134780645370483,
            46.199419891797504
          ],
          [
            6.135174930095673,
            46.199341919067216
          ],
          [
            6.135663092136382,
            46.199371622977516
          ],
          [
            6.13562285900116,
            46.200127210796296
          ],
          [
            6.135263442993164,
            46.20020518241219
          ],
          [
            6.134834289550781,
            46.20008822494683
          ],
          [
            6.134630441665649,
            46.19988029994932
          ]
      ];

      let coords = points.map((p) => { return { longitude: p[0], latitude: p[1] } });

      let dyn_sequencer:I_AssetSequencer = this.nora_service.dynamic_asset_sequencer();

      for(let c of coords){
        let asset = dyn_sequencer.next_asset(c);
        let ambient_audio:AudioAttachment = dyn_sequencer.next_ambient_audio(c);
        console.log('prog', dyn_sequencer.progression);
        console.log("asset", asset);
        console.log("ambient audio", ambient_audio);
      }
  }

  test_static_playlist():void{

      let playlist_infos:ReadonlyArray<PlaylistInfo> = this.nora_service.available_playlists();

      let play_list_id:string = playlist_infos[1].playlist_id;
      let sequencer:I_AssetSequencer = this.nora_service.static_asset_sequencer_with_playlist_id(play_list_id);
      console.log(sequencer.title);

      let asset = sequencer.next_asset();

      while(asset){

        console.log( 'prog',sequencer.progression);
        console.log('asset', asset.title);
        console.log('url', asset.audio_file[0].url);
        // console.log('next ambient', sequencer.next_ambient_audio());
        asset = sequencer.next_asset();
      }
      console.log("all done");
  }
}
