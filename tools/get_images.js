var Airtable = require('airtable');
Airtable.configure({
    endpointUrl: 'https://api.airtable.com',
    apiKey: 'keyaKzWmgR1nyCzJX'
});
var base = Airtable.base('appIkePsadBelwo2q');

var filepaths = [];

base('LLDD').select({
    // Selecting the first 3 records in Regarder:
    maxRecords: 500,
    view: "Regarder"
}).eachPage(function page(records, fetchNextPage) {
    // This function (`page`) will get called for each page of records.

    records.forEach(function(record) {
        var attachment = record.get('Image attachée');
        if(attachment){
            filepaths.push(attachment[0].url);
        }
    });

    // To fetch the next page of records, call `fetchNextPage`.
    // If there are more records, `page` will get called again.
    // If there are no more records, `done` will get called.
    fetchNextPage();

}, function done(err) {
    if (err) { console.error(err); return; }

    filepaths = filepaths.map(function(fp){
        return "wget " + fp;
    });
    var result = filepaths.join('\n');
    console.log(result);
});