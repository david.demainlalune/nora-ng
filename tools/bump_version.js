// naive version bump script
// increment last digit of version
// expects version to be present
// expects version to be a dot separated value

var fs = require('fs'),
    _ = require('lodash'),
    util = require('util'),
    package = JSON.parse(fs.readFileSync('./package.json'));

_.mixin({
    inc_last: function(a){ a[a.length - 1] += 1; return a; } 
});

var result =_.chain(package.version)
    .split('.')
    .map(_.parseInt)
    .inc_last()
    .map(_.toString)
    .join('.')
    .value();
    
package.version = result;

fs.writeFileSync('./package.json', JSON.stringify(package, null, 2), 'utf-8');

console.log(util.format("version bumped to %s", result));