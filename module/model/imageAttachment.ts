// type id = string;

import { id } from './asset'

export class ImageAttachment
{
    public id:id;
    public url:string;
    public filename:string;
    public size:number;
    public thumbnails:Thumbnails;
    public type:string;
}

export class Thumbnails
{
    public small:ImagePayload;
    public large:ImagePayload;
}


export class ImagePayload
{
    public width:number;
    public height:number;
    public url:string;
}