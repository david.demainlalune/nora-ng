import { ImageAttachment } from './imageAttachment'

// struct like object containing the name of a static playlist
// and its id
export class PlaylistInfo
{
    public readonly playlist_id:string;
    public readonly title:string;
    public readonly image:ImageAttachment;
    public readonly asset_count:number;

    constructor(playlist_id:string, title:string, image:ImageAttachment, asset_count:number){
        this.playlist_id = playlist_id;
        this.title = title;
        this.image = image;
        this.asset_count = asset_count;
    }
}