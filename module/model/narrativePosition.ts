export enum NarrativePosition { INIT=0, DEVELOPMENT=1, DISPARITION=2, CONCLUSION=3 }

export function convert_narrative_position(name:string):NarrativePosition{
    switch(name){
        case 'Initiateur':
            return NarrativePosition.INIT;
        case 'Développement':
            return NarrativePosition.DEVELOPMENT;
        case 'Disparition':
            return NarrativePosition.DISPARITION;
        case 'Conclusion':
            return NarrativePosition.CONCLUSION;
        default:
            throw new Error("invalid narrative position " + name);
    }
}