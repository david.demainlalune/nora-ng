// let airtable = require("airtable");

//const airtable = require('../../../node_modules/airtable/build/airtable.browser.js');
// const airtable:any = require('airtable');

// Get a base ID for an instance of art gallery example
// var base = new Airtable({ apiKey: 'FILL_OUT_API_KEY' }).base('FILL_OUT_BASE_ID');


import * as _ from 'lodash';

import * as Airtable from 'airtable';

export class AirtableWrapper
{

    base:any;

    constructor(config:any){
        // Airtable.configure(config.auth);
        // this.base = airtable.base(config.base_id);
        this.base = new Airtable( config.auth ).base(config.base_id);
    }


    // promisify the extraction of one table
    public extract_table(table_name:string, view_name:string):Promise<Array<Object>>{
        var result:Array<Object> = [];

        return new Promise((resolve, reject) => {
            this.base(table_name)
                .select({
                    maxRecords: 500,
                    view: view_name
                })
                .eachPage(function page(records:any, fetchNextPage:any) {

                    // This function (`page`) will get called for each page of records.
                    records.forEach(function (record:any) {
                        // parse the record and only keep the fields we want
                        // var transformed_record = this.parse_record(record, translation_dict);
                        result.push(record);
                    });

                    // To fetch the next page of records, call `fetchNextPage`.
                    // If there are more records, `page` will get called again.
                    // If there are no more records, `done` will get called.
                    fetchNextPage();

                }, function done(error:any) {
                    if (error) {
                        console.log(error);
                        reject(error);
                    }

                    resolve(result);
                });
        });
    };
}