
import { NarrativePosition } from './narrativePosition'
import * as _ from 'lodash'

export class NarrativeArcSequence
{

    private static MIN_ITEMS_PER_POS:number = 1;
    private static MAX_ITEMS_PER_POS:number = 3;

    private arcs:Array<NarrativePosition>;

    private start_count:number;
    public get progression():string{
        return (this.start_count - this.arcs.length) + " / " + this.start_count;
    }

    public next():NarrativePosition{
        return this.arcs.shift();
    }

    constructor(){
        // get count of items in enum NarrativePosition
        // https://stackoverflow.com/questions/38034673/determine-the-number-of-enum-elements-typescript
        let count_of_positions = Object.keys(NarrativePosition).length / 2;
        this.arcs =_.chain(_.range(count_of_positions))
                    .map((n) =>{
                        // map to array [NarrativePosition, count]
                        let count = _.random(NarrativeArcSequence.MIN_ITEMS_PER_POS, NarrativeArcSequence.MAX_ITEMS_PER_POS);
                        return [NarrativePosition[n], count];
                    })
                    .map((arr_pair) => _.fill(Array(arr_pair[1]), arr_pair[0]))
                    .flatten()
                    .value();

        this.start_count = this.arcs.length;
    } 
}
