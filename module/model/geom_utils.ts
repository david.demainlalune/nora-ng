
import * as _ from 'lodash';
export class GeomUtils
{

    public static convert_position_to_turf_point(p:any):GeoJSON.Feature<GeoJSON.Point>{
        let lat = _.get(p, 'coords.latitude'),
            longi = _.get(p, 'coords.longitude');

        if(lat !== undefined && longi !== undefined){
            return turf.point([longi, lat]);
        }else{
            return p;
        }
    }
}