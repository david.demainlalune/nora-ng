
import { id } from './asset'

export class AudioAttachment
{
    public id:id;
    public url:string;
    public filename:string;
    public size:number;
    public type:string;
}