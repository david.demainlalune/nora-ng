import { ImageAttachment } from './imageAttachment'
import * as _ from 'lodash';

import { PlaylistInfo } from './playlistInfo'

import { id } from './asset'

export class Playlist
{
    public id:string;
    public title:string;
    public assets:Array<id>;
    public themes:Array<string>;
    public images:Array<ImageAttachment>;

    public get image():ImageAttachment{
        if(_.isUndefined(this.images)||this.images.length == 0){
            return null;
        }
        return this.images[0];
    }

    public toPlaylistInfo():PlaylistInfo{
        let imageClone = null;
        if(this.image){
            // clone it
            imageClone = Object.assign({}, this.image);
        }
        return new PlaylistInfo(this.id, this.title, imageClone, this.assets.length);
    }

    public static NewFromAirtable(record:any):Playlist{
        let result:Playlist = new Playlist();
        result.id = record.getId();
        result.title = record.get('Playlists');
        result.assets = record.get('Assets');
        result.themes = record.get('Thème');
        result.images = record.get('Image de la playlist');

        return result;
    }
}