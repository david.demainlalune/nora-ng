import { Asset } from '../model/asset';
import { NarrativeArcSequence } from '../model/narrativeArcSequence';
import { NarrativePosition } from '../model/narrativePosition';
import { Zone } from '../data/zone_data'
import { AudioAttachment } from '../model/audioAttachment';
import { AmbientAudio } from '../model/ambientAudio.model';
import * as GeoJSON from 'geojson';
import * as turf from 'turf';
import * as _ from 'lodash';

export interface I_GpsPosition{
    latitude:number;
    longitude:number;
}

export interface I_AssetSequencer{
    id:string; // only dynamicAssetSequencers have an id ??
    title:string;
    progression:string;
    next_asset(gps_position?:I_GpsPosition):Asset;
    next_ambient_audio(gps_position?:I_GpsPosition):AudioAttachment;
}

export class StaticAssetSequencer implements I_AssetSequencer
{
    public readonly id:string;
    public readonly title:string;

    private assets:ReadonlyArray<Asset>;
    private ambient_audio_attachments:AudioAttachment[];

    private current_index:number = 0;

    public get progression():string{
        return this.current_index + " / "  + this.assets.length;
    }

    constructor(id:string, title:string, assets:Array<Asset>, ambient_audios:AmbientAudio[]){
        this.id = id;
        this.title = title;
        this.assets = assets;
        console.log('playlist_id', id);
        
        // define ambient audios
        // are there any linked to this playlist?
        this.ambient_audio_attachments = _.filter(ambient_audios, ((aa:AmbientAudio) => {
            return aa.playlist_ids.indexOf(id) >= 0;
        }));
    }

    public next_asset():Asset{
        if(this.current_index > this.assets.length - 1){
            return null;
        }
        let result:Asset = this.assets[this.current_index];
        this.current_index += 1;
        return result;
    }

    public next_ambient_audio():AudioAttachment{
        let result:AmbientAudio[] = _.chain(this.ambient_audio_attachments)
                                    .shuffle()
                                    .take(1)
                                    .value();

        return _.isEmpty(result) ? null : result[0].audio_attachment;
    }
}

export class DynamicAssetSequencer implements I_AssetSequencer
{

    public readonly id:string = "dyn";
    public readonly title:string = "dyn";

    private assets:Asset[];
    private zones:GeoJSON.FeatureCollection<GeoJSON.Polygon>;
    private narrative_arc_sequence:NarrativeArcSequence;
    private ambient_audios:AmbientAudio[];

    constructor(assets:Array<Asset>, zones:GeoJSON.FeatureCollection<GeoJSON.Polygon>, ambient_audios:AmbientAudio[] ){
        this.assets = _.filter(assets, (a) => !_.isUndefined(a.audio_file));
        this.zones = zones;
        this.ambient_audios = ambient_audios;
        this.narrative_arc_sequence = new NarrativeArcSequence();
    }

    public get progression():string{
        return this.narrative_arc_sequence.progression;
    }

    public next_asset(gps_position?:I_GpsPosition):Asset{
        if(_.isUndefined(gps_position) || _.isNull(gps_position)){
            throw new Error('dynamic asset sequencer needs a gps position to return next asset');
        }

        let next_narrative_position:NarrativePosition = this.narrative_arc_sequence.next();
        console.log("next narr pos", next_narrative_position, NarrativePosition[next_narrative_position]);

        if(_.isUndefined(next_narrative_position)){
            // sequence terminated
            return null;
        }

        let current_zone:Zone = this.zone_for_point(this.format_point(gps_position));
        console.log("next zone", current_zone);
        
        let asset:Asset = _.chain(this.assets)
                           .filter((a) =>  a.narrative_position == NarrativePosition[next_narrative_position] && a.zone == current_zone )
                           .sample()
                           .value();

        // console.log("asset 0", asset);

        if(_.isUndefined(asset)){
            // no asset with narr. position and zone
            // is there maybe a asset with narr. position in any zone?
            asset = _.chain(this.assets)
                     .filter({ narrative_position: NarrativePosition[next_narrative_position] })
                     .sample()
                     .value();
            // console.log("asset 1", asset);
        }

        // abort before end of narrative arc
        if(_.isUndefined(asset)){
            console.log("abort story");
            return null;
        }

        // remove asset from collection
        let index:number = this.assets.indexOf(asset);
        // console.log("index", index);
        this.assets.splice(index, 1);
        // console.log("len", this.assets.length);

        return asset;
    }

    public next_ambient_audio(gps_position:I_GpsPosition):AudioAttachment{
        if(_.isUndefined(gps_position) || _.isNull(gps_position)){
            throw new Error('dynamic asset sequencer needs a gps position to return next asset');
        }

        let current_zone:Zone = this.zone_for_point(this.format_point(gps_position));
        // console.log("next ambient zone", current_zone);
        
        let result:AmbientAudio[] = _.chain(this.ambient_audios)
                                    .filter((aa:AmbientAudio) => {
                                        return aa.zone == current_zone;})
                                    .shuffle()
                                    .take(1)
                                    .value();

        return _.isEmpty(result) ? null : result[0].audio_attachment;
    }

    public test_zone_for_point():void{        
        console.log(this.zone_for_point(turf.point([ 6.13511323928833, 46.19984317040266])));
    }

    // ensure convert gps_position to turf point type
    private format_point(point:I_GpsPosition):GeoJSON.Feature<GeoJSON.Point>{
        return turf.point([point.longitude, point.latitude]);
    }

    private zone_for_point(geo_point:GeoJSON.Feature<GeoJSON.Point>):Zone{

        let includes_point = _.partial(turf.inside, geo_point),
            result =  _.find(this.zones.features, includes_point);

        if(result === undefined){
            console.log("in free zone");
            return Zone.free;
        }
        let zone_name:string = result.properties.zone_name;

        return Zone[zone_name];
    }   
}