

import { id } from './asset'
import { AudioAttachment } from './audioAttachment';
import { Zone, zone_from_record } from '../data/zone_data';

import * as _ from 'lodash';

export class AmbientAudio
{
    public id:id;
    public title:string;
    public audio_attachment:AudioAttachment;

    public zone:Zone;
    public playlist_ids:id[];

    public static NewFromAirtable(record:any, airtable_zones:any[]):AmbientAudio{

        // TODO extract zone creation logic (duplicated in asset)
        // 2 translate zone
        // let zone_record = record.get('Zone');
        // let zone:Zone = Zone.null;
        // if(!_.isUndefined(zone_record)){
        //     let zone_name = _.find(airtable_zones, { id: zone_record[0] }).name;
        //     zone = convert_zone(zone_name);
        // }

        let default_zone:Zone = Zone.null;
        let result:AmbientAudio = new AmbientAudio();
        result.id = record.getId();
        result.title = record.get('Titre');
        result.audio_attachment = record.get('Fichier audio') ? <AudioAttachment>record.get('Fichier audio')[0] : null;
        result.zone = zone_from_record(record, airtable_zones, default_zone);
        result.playlist_ids = record.get('Playlists') || [];

        return result;
    } 
}