import { ImageAttachment } from './imageAttachment'
import { AudioAttachment } from './audioAttachment'
import * as _ from 'lodash'
import { NarrativePosition, convert_narrative_position } from './narrativePosition'
import { Zone, zone_from_record } from '../data/zone_data'

export type id = string;

export class Asset
{
    public id:id;
    public title:string;

    public audio_file:Array<AudioAttachment>;
    public creation_date:Date;
    public zone:Zone;
    public speed:number;
    public tags:Array<id>;
    public themes:Array<id>;
    public narrative_position:NarrativePosition;
    public literary_form:Array<id>;
    public characters:Array<id>;
    public doc_url:string;
    public images:Array<ImageAttachment>;
    public images_md:Array<ImageAttachment>;
    public text:string;

    public clone():Asset{
        return Object.assign({}, this);
    }


    public static NewFromAirtable(record:any, narrative_position_dict:any, airtable_zones:any[]):Asset{
        // 1 translate narrative position 
        let pos_in_histoire = record.get('Position dans l\'histoire');
        let narrative_position:NarrativePosition;
        if(!_.isUndefined(pos_in_histoire)){
            let narrative_position_name = _.find(narrative_position_dict, { id: pos_in_histoire[0] }).name;
            narrative_position = convert_narrative_position(narrative_position_name);
        }

        // // TODO similar code in AmbienAudio creation
        // // 2 translate zone
        // let zone_record = record.get('Zone');
        // let zone:Zone = Zone.free;
        // if(!_.isUndefined(zone_record)){
        //     let zone_name = _.find(airtable_zones, { id: zone_record[0] }).name;
        //     zone = convert_zone(zone_name);
        // }

        let default_zone:Zone = Zone.free;
        let result:Asset = new Asset();
        result.id = record.getId();
        result.title = record.get('Titre du document');
        result.audio_file = <Array<AudioAttachment>> record.get('Document audio attaché');
        result.creation_date = record.get('Date de création');
        result.zone = zone_from_record(record, airtable_zones, default_zone);
        result.speed = record.get('Vitesse (1 à 10)');
        result.tags = record.get('Tags');
        result.themes = record.get('themes');
        result.narrative_position = narrative_position;
        result.literary_form = record.get('Forme littéraire');
        result.characters = record.get('Personnages');
        result.doc_url = record.get('url du document');
        result.images = <Array<ImageAttachment>> record.get('Image attachée');
        result.images_md = <Array<ImageAttachment>> record.get('image-md');
        result.text = record.get('Texte complet');

        return result;
    }
}
