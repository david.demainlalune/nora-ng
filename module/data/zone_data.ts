
import * as _ from 'lodash';

export function get_zone_features(dev:boolean = false):GeoJSON.FeatureCollection<GeoJSON.Polygon>{
    return dev ? zone_dev : zone_prod; 
};

// fixme:
// we allow null value to support AmbientAudio files that can be linked to a Zone or a Playlist
// i.e. can be linked to a playlist while not being linked to a zone.
// this is error prone…
export enum Zone { null=-1, free=0, zone_1=1, zone_2=2, zone_3=3, zone_4=4}

export function zone_from_record(record:any, airtable_zones:any[], default_zone:Zone=Zone.free):Zone{
    let zone_record = record.get('Zone');
    let zone: Zone = default_zone;
    if (!_.isUndefined(zone_record)) {
        let zone_name = _.find(airtable_zones, { id: zone_record[0] }).name;
        zone = convert_zone(zone_name);
    }
    return zone;
}

function convert_zone(zone_name:string):Zone{
    switch(zone_name){
        case "Zone 1":
            return Zone.zone_1;
        case "Zone 2":
            return Zone.zone_2;
        case "Zone 3":
            return Zone.zone_3;
        case "Zone 4":
            return Zone.zone_4;
        case "Libre":
            return Zone.free;
        default:
            return Zone.null;
    }
}

let zone_prod:GeoJSON.FeatureCollection<GeoJSON.Polygon> = {
    type: "FeatureCollection",
    features: [
        {
            type: "Feature",
            geometry: {
                type: "Polygon",
                coordinates: [
                    [
                        [
                            2.35651709139347,
                            48.85186209223
                        ],
                        [
                            2.35578753054142,
                            48.8510643441402
                        ],
                        [
                            2.3564688116312,
                            48.8508172514457
                        ],
                        [
                            2.35784210264683,
                            48.8502136341641
                        ],
                        [
                            2.35905982553959,
                            48.8499277076487
                        ],
                        [
                            2.36007370054722,
                            48.8494229198147
                        ],
                        [
                            2.36035265028477,
                            48.8493487897043
                        ],
                        [
                            2.3604492098093,
                            48.8494335098216
                        ],
                        [
                            2.36049748957157,
                            48.8498112186002
                        ],
                        [
                            2.3602507263422,
                            48.8503618917144
                        ],
                        [
                            2.36018635332584,
                            48.8505525079199
                        ],
                        [
                            2.35996641218662,
                            48.8506160464937
                        ],
                        [
                            2.35845901072025,
                            48.851170240636
                        ],
                        [
                            2.35778845846653,
                            48.8514243913121
                        ],
                        [
                            2.35754169523716,
                            48.8514949884931
                        ],
                        [
                            2.35651709139347,
                            48.85186209223
                        ]
                    ]
                ]
            },
            properties: {
                zone_name: "zone_3",
                zone_id:3
            }
        },
        {
            type: "Feature",
            geometry: {
                type: "Polygon",
                coordinates: [
                    [
                        [
                            2.35311739146709,
                            48.8528724960674
                        ],
                        [
                            2.3535143584013,
                            48.8529413263293
                        ],
                        [
                            2.3547749966383,
                            48.852496575275
                        ],
                        [
                            2.35651843249798,
                            48.8518612097721
                        ],
                        [
                            2.35578887164593,
                            48.8510634616685
                        ],
                        [
                            2.35520951449871,
                            48.8511834776822
                        ],
                        [
                            2.35418491065502,
                            48.8514376282911
                        ],
                        [
                            2.35358946025372,
                            48.8516370650183
                        ],
                        [
                            2.35327295958996,
                            48.8521435954356
                        ],
                        [
                            2.35311739146709,
                            48.8528724960674
                        ]
                    ]
                ]
            },
            properties: {
                 zone_name: "zone_2",
                 zone_id:2
            }
        },
        {
            type: "Feature",
            geometry: {
                type: "Polygon",
                coordinates: [
                    [
                        [
                            2.35286258161068,
                            48.8537408093615
                        ],
                        [
                            2.35323809087276,
                            48.8538749377145
                        ],
                        [
                            2.35446117818356,
                            48.8534301949538
                        ],
                        [
                            2.35713265836239,
                            48.8525795251761
                        ],
                        [
                            2.35651776194572,
                            48.8518616510007
                        ],
                        [
                            2.3548661917448,
                            48.852461277403
                        ],
                        [
                            2.3535143584013,
                            48.8529413263293
                        ],
                        [
                            2.35299132764339,
                            48.8528477877452
                        ],
                        [
                            2.35286258161068,
                            48.8537408093615
                        ]
                    ]
                ]
            },
            properties: {
                zone_name: "zone_1",
                zone_id: 1
            }
        },
        {
            type: "Feature",
            geometry: {
                type: "Polygon",
                coordinates: [
                    [
                        [
                            2.35651843249798,
                            48.8518612097724
                        ],
                        [
                            2.35713265836239,
                            48.8525795251762
                        ],
                        [
                            2.35930323600769,
                            48.8518872422668
                        ],
                        [
                            2.36010253429413,
                            48.8514354221277
                        ],
                        [
                            2.36036539077759,
                            48.8510400761612
                        ],
                        [
                            2.3601870238781,
                            48.8505529491603
                        ],
                        [
                            2.35898271203041,
                            48.8509730082328
                        ],
                        [
                            2.35778912901878,
                            48.8514248325448
                        ],
                        [
                            2.35754236578941,
                            48.8514954297253
                        ],
                        [
                            2.35682286322117,
                            48.8517535498189
                        ]
                    ]
                ]
            },
            properties: {
                zone_name: "zone_4",
                zone_id: 4
            }
        }
    ]
};

let zone_dev:GeoJSON.FeatureCollection<GeoJSON.Polygon> = {
  type: "FeatureCollection",
  features: [
    {
      type: "Feature",
      properties: {
        zone_name: "zone_2",
        zone_id: 2
      },
      geometry: {
        type: "Polygon",
        coordinates: [
          [
            [
              6.135083734989166,
              46.19977448067522
            ],
            [
              6.134885251522064,
              46.19986173517912
            ],
            [
              6.134533882141113,
              46.199527568243155
            ],
            [
              6.1349791288375854,
              46.199278798204496
            ],
            [
              6.135317087173462,
              46.199666804713445
            ],
            [
              6.135083734989166,
              46.19977448067522
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        zone_name: "zone_3",
        zone_id: 3
      },
      geometry: {
        type: "Polygon",
        coordinates: [
          [
            [
              6.135322451591492,
              46.19965937877727
            ],
            [
              6.134984493255614,
              46.199276941707446
            ],
            [
              6.135569214820862,
              46.199000322944556
            ],
            [
              6.135695278644562,
              46.19909314818803
            ],
            [
              6.13587498664856,
              46.199327067106026
            ],
            [
              6.135958135128021,
              46.199512716332165
            ],
            [
              6.135531663894653,
              46.19961853611037
            ],
            [
              6.135322451591492,
              46.19965937877727
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        zone_name: "zone_1",
        zone_id: 1
      },
      geometry: {
        type: "Polygon",
        coordinates: [
          [
            [
              6.134887933731079,
              46.19986730461084
            ],
            [
              6.135038137435913,
              46.2001253543279
            ],
            [
              6.135413646697998,
              46.200036243771216
            ],
            [
              6.1355799436569205,
              46.200015822581626
            ],
            [
              6.135424375534058,
              46.1998376009685
            ],
            [
              6.135317087173462,
              46.199676087132254
            ],
            [
              6.134887933731079,
              46.19986730461084
            ]
          ]
        ]
      }
    },
    {
      type: "Feature",
      properties: {
        zone_name: "zone_4",
        zone_id: 4
      },
      geometry: {
        type: "Polygon",
        coordinates: [
          [
            [
              6.135593354701996,
              46.20001210963726
            ],
            [
              6.135958135128021,
              46.19994898954445
            ],
            [
              6.136167347431183,
              46.19990072118926
            ],
            [
              6.136049330234528,
              46.19965009635566
            ],
            [
              6.135963499546051,
              46.19952385526578
            ],
            [
              6.135550439357758,
              46.19962781853733
            ],
            [
              6.1353278160095215,
              46.199664948229525
            ],
            [
              6.135437786579132,
              46.199845026880595
            ],
            [
              6.135593354701996,
              46.20001210963726
            ]
          ]
        ]
      }
    }
  ]
}


