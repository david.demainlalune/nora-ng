import { Injectable } from '@angular/core';

import { AirtableWrapper } from './model/airtableWrapper'
import { Asset } from './model/asset'
import { Playlist } from './model/playlist'
import { get_zone_features } from './data/zone_data'
import { NarrativePosition } from './model/narrativePosition'
import { PlaylistInfo } from './model/playlistInfo'
import { AmbientAudio } from './model/ambientAudio.model'
import { I_AssetSequencer, StaticAssetSequencer, DynamicAssetSequencer } from './model/i_AssetSequencer'

import * as _ from 'lodash';

import * as GeoJSON from 'geojson';
import * as turf from 'turf';

@Injectable()
export class NoraService {
    
    airtableWrapper:AirtableWrapper;
    zone_features:GeoJSON.FeatureCollection<GeoJSON.Polygon>;

    assets:Asset[];
    playlists:Playlist[];
    ambient_audios:AmbientAudio[];

    inited:boolean = false;

    constructor(){
        let airtable_config = {
            auth: {
                apiKey: 'keyaKzWmgR1nyCzJX',
            },
            base_id: 'appIkePsadBelwo2q'
        };

        let dev_mode:boolean = false;
        this.zone_features = get_zone_features(dev_mode);

        this.airtableWrapper = new AirtableWrapper(airtable_config);
    }

    public init():Promise<void>{
        let narrative_position_dict = null,
            airtable_zones:any[] = null;

        // return if already initialized
        if(this.inited){
            return Promise.resolve(null);
        }
        
        return this.airtableWrapper.extract_table("Position dans l'histoire", 'Main View')
            .then(records => {
                narrative_position_dict = records.map( (o:any) => {
                    return {
                        id: o.getId(),
                        name: o.get('name')
                    };
                });
                return this.airtableWrapper.extract_table('Zones', 'Main View')
            })
            .then(records => {
                airtable_zones = records.map((record:any) => {
                    return {
                        id: record.getId(),
                        name: record.get('name')
                    };
                });
                return this.airtableWrapper.extract_table('LLDD', 'Regarder')
            })
            .then(records => {
                this.assets = records.map(o => Asset.NewFromAirtable(o, narrative_position_dict, airtable_zones));
                return this.airtableWrapper.extract_table('Playlists', 'Main View');
            })
            .then(records => {
                this.playlists = records.map(o => Playlist.NewFromAirtable(o));
                for(let playlist of this.playlists){
                    playlist.assets.reverse();
                }
                return this.airtableWrapper.extract_table('Pistes ambiance', 'Grid view');
            })
            .then(records => {
                this.ambient_audios = records.map(o => AmbientAudio.NewFromAirtable(o, airtable_zones));
                this.inited = true;
            });
    }

    public available_playlists():ReadonlyArray<PlaylistInfo>{
        if(!this.inited) throw new Error("noraservice not initialized");
        return this.playlists.map((p:Playlist) => p.toPlaylistInfo());
    }

    public static_asset_sequencer_with_playlist_id(playlist_id:string):I_AssetSequencer {
        if(!this.inited) throw new Error("noraservice not initialized");
        
        let playlist:Playlist = _.find(this.playlists, { id: playlist_id });
        if(!playlist) return null;

        let assets:Array<Asset> = playlist.assets.map((asset_id:string) => {
            return _.find(this.assets, { id: asset_id }).clone();
        });

        return new StaticAssetSequencer(playlist.id, playlist.title, assets, this.ambient_audios);
    }

    public dynamic_asset_sequencer():I_AssetSequencer{
        if(!this.inited) throw new Error("noraservice not initialized");
        return new DynamicAssetSequencer(this.assets, this.zone_features, this.ambient_audios);
    }
}