import { NoraNgPage } from './app.po';

describe('nora-ng App', () => {
  let page: NoraNgPage;

  beforeEach(() => {
    page = new NoraNgPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
