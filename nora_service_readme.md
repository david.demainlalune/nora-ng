# nora.service

the nora service is a factory base service. It generates "iterable-like" sequences of assets.
Two types of sequencers are supported, static and dynamic

## initialization

the nora.service is expected to run as an Angular service (i.e. dependency injected).

The service is bootstrapped by calling the method **init** which returns a void Promise.
note: the service is only initialized once. Subsequent calls to **init** return an immediately resolved Promise.

## sequencer interface

            export interface I_AssetSequencer{
                id:string; // playlist id when concrete implementation is Static Sequencer 
                title:string; // playlist title when concrete implementation is Static Sequencer 
                progression:string; // 'n of tot' string when concrete implementation is Static Sequencer 
                next_asset(gps_position?:any):Asset; // returns next asset or null when sequence complete
                next_ambient_audio(gps_position?:any):AudioAttachment; // returns next ambient audio file or null when no ambient file available
            }

## static sequencer

static sequencers are based on a set of fixed playlists.
the following example illustrates how to retrieve such a sequencer.

            this.nora_service.init()
            .then(() => {
                let playlist_infos:ReadonlyArray<PlaylistInfo> = this.nora_service.available_playlists();

                let play_list_id:string = playlist_infos[0].playlist_id;
                let sequencer:I_AssetSequencer = this.nora_service.static_asset_sequencer_with_playlist_id(play_list_id);
                console.log(sequencer.title);

                let asset:Asset = sequencer.next_asset();
                let ambientAudio:AudioAttachment = sequencer.next_ambient_audio();
                while(asset){
                    console.log(sequencer.progression);
                    console.log(asset);
                    asset = sequencer.next_asset();
                });

## dynamic sequences

dynamic sequences are based on the user's location and a sequence describing a narrative arc.
the following example illustrates how to retrieve such a sequencer.

            this.nora_service.init()
            .then(() => {
                let dyn_sequencer:I_AssetSequencer = this.nora_service.dynamic_asset_sequencer();

                let gps_coordinates = {
                    coords:{
                        longitude:42,
                        lattitude:42
                    }
                };

                let asset:Asset = dyn_sequencer.next_asset(gps_coordinates);
                let ambient_audio:AudioAttachment = dyn_sequencer.next_ambient_audio(gps_coordinates);
                if(asset){
                    // do something
                }else{
                    // all done
                }
            });
